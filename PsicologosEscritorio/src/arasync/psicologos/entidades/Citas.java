package arasync.psicologos.entidades;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by Juan on 02/03/2017.
 */

@Entity
@Table(name = "citas")

public class Citas {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name="id_cita")
    private int id;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha")
    private Date fecha;

    @ManyToOne
    @JoinColumn(name = "id_paciente")
    private Pacientes pacientes;

    public Citas(String descripcion, Date fecha, Pacientes pacientes) {
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.pacientes = pacientes;
    }

    public Citas() {

    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Pacientes getPacientes() {
        return pacientes;
    }

    public void setPacientes(Pacientes pacientes) {
        this.pacientes = pacientes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        SimpleDateFormat dateFormatYouWant = new SimpleDateFormat("dd MMMMM, yyyy");
        String sCertDate = dateFormatYouWant.format(fecha);

        return pacientes.getNombre() + " " + pacientes.getApellido() + "   -   Día: " + sCertDate + "   -   " + "Descripcion: " + descripcion;

    }
}
