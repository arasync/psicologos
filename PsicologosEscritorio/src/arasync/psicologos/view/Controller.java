package arasync.psicologos.view;

import arasync.psicologos.entidades.Citas;
import arasync.psicologos.entidades.Pacientes;
import arasync.psicologos.hibernate.HibernateControlador;
import arasync.psicologos.util.FormatFechas;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;


/**
 * Created by Juan on 27/02/2017.
 */
public class Controller implements Initializable {
    private HibernateControlador hibernateControlador;
    private ObservableList data;
    private Citas citas;

    /* MENU LATERAL */

    @FXML
    private Button bt_acordeon_inicio;

    @FXML
    private Button bt_acordeon_calendario;

    @FXML
    private Button bt_acordeon_pacientes;

    @FXML
    private Button bt_acordeon_acercade;


    /* PANEL INICIO */
    @FXML
    private AnchorPane layout_inicio;

    @FXML
    private AnchorPane layout_calendario;

    @FXML
    private AnchorPane layout_acercade;

    @FXML
    private AnchorPane layout_pacientes;

    @FXML
    private Button bt_cerrar_programa;


    /* BOTONES PARA LA PANTALLA DE INICIO */

    @FXML
    private Button bt_anchor_inicio_calendario;

    @FXML
    private Button bt_anchor_inicio_pacientes;

    @FXML
    private Button bt_anchor_inicio_acercade;

    /* FIN BOTONES PARA LA PANTALLA DE INICIO */


    /* CONTROLES LAYOUT CITAS */
    @FXML
    private ComboBox cb_pacientes_cita;
    @FXML
    private TextArea ta_descripcion_insertarCita;
    @FXML
    private DatePicker dp_insertarCita_fecha;
    @FXML
    private Button bt_aceptar_insertarCita;
    @FXML
    private Button bt_vaciar_insertarCita;

    @FXML
    private DatePicker dp_cita_desde;
    @FXML
    private ListView lv_citas;
    @FXML
    private Button bt_eliminar_cita;
    @FXML
    private Button bt_vertodo_citas;
    @FXML
    private Button bt_verhoy_citas;

    /* FIN CONTROLES LAYOUT CITAS */


    /* CONTROLES LAYOUT PACIENTES */
    @FXML
    private TableView tabla_pacientes;
    @FXML
    private TableColumn col_nombre_paciente;
    @FXML
    private TableColumn col_apellidos_paciente;

    @FXML
    private TableColumn col_fecha_inscr;

    @FXML
    private TableColumn col_fecha_paciente;

    @FXML
    private TableColumn col_tf_paciente;

    @FXML
    private TableColumn col_email_paciente;

    @FXML
    private TextField tf_nombre_paciente;

    @FXML
    private TextField tf_apellidos;

    @FXML
    private DatePicker dp_nacimiento;

    @FXML
    private TextField tf_telefono;

    @FXML
    private TextField tf_email;

    @FXML
    private Button bt_eliminar_fila_paciente;

    @FXML
    private Button bt_modificar_fila_paciente;

    @FXML
    private Button bt_vaciar_form_paciente;

    @FXML
    private Button bt_insertar_paciente;

    /* FIN CONTROLES LAYOUT PACIENTES */


    private Date fecha_desde_cita;


    @FXML
    private void CerrarPrograma() {
        System.exit(0);

    }


    /* FUNCIONES LAYOUT PACIENTES */
    @FXML
    private void vaciarFormPaciente() {
        tf_nombre_paciente.setText("");
        tf_apellidos.setText("");
        dp_nacimiento.setValue(null);
        tf_telefono.setText("");
        tf_email.setText("");

        tabla_pacientes.getSelectionModel().select(null);

    }

    @FXML
    private void insertarPaciente() {
        if (tf_nombre_paciente.getText().equalsIgnoreCase("") || tf_apellidos.getText().equalsIgnoreCase("") ||
                dp_nacimiento.getValue()==null || tf_telefono.getText().equalsIgnoreCase("") ||
                tf_email.getText().equalsIgnoreCase("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error enviando datos");
            alert.setHeaderText("Hay campos sin rellenar");
            alert.setContentText("Por favor, rellena todos los campos");
            alert.showAndWait();
            return;
        }
        LocalDate localDate = dp_nacimiento.getValue();
        Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
        Date fecha = Date.from(instant);
        Date fecha_insc = new Date();
        fecha_insc.getTime();
        Pacientes pacientes;
        pacientes = new Pacientes();
        pacientes.setNombre(tf_nombre_paciente.getText());
        pacientes.setApellido(tf_apellidos.getText());
        pacientes.setFecha_nacimiento(fecha);
        pacientes.setTelefono(Integer.parseInt(tf_telefono.getText()));
        pacientes.setEmail(tf_email.getText());
        pacientes.setFecha_inscripcion(fecha_insc);

        hibernateControlador.guardarPaciente(pacientes);
        vaciarFormPaciente();
        rellenarTabla();

    }

    @FXML
    private void eliminarPaciente() {
        Pacientes filaSeleccionada = (Pacientes) tabla_pacientes.getSelectionModel().getSelectedItem();
        int numeroFila = tabla_pacientes.getSelectionModel().getSelectedIndex();
        if (numeroFila >= 0) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Eliminar");
            alert.setHeaderText("Eliminar paciente");
            alert.setContentText("¿Estás seguro?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                hibernateControlador.eliminarPaciente(filaSeleccionada);
                rellenarTabla();
                vaciarFormPaciente();
            } else {
                return;
            }

        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Sin selección");
            alert.setHeaderText("No has seleccionado ningún paciente");
            alert.setContentText("Por favor, selecciona un paciente en la lista.");
            alert.showAndWait();
        }
    }

    @FXML
    private void modificarPaciente() {
        Pacientes pacientes = (Pacientes) tabla_pacientes.getSelectionModel().getSelectedItem();
        int numeroFila = tabla_pacientes.getSelectionModel().getSelectedIndex();

        if (numeroFila >= 0) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Modificar");
            alert.setHeaderText("Modificar paciente");
            alert.setContentText("¿Estás seguro?");

            LocalDate localDate = dp_nacimiento.getValue();
            Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
            Date fecha = Date.from(instant);
            Date fecha_insc = new Date();
            fecha_insc.getTime();

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                pacientes.setNombre(tf_nombre_paciente.getText());
                pacientes.setApellido(tf_apellidos.getText());
                pacientes.setFecha_nacimiento(fecha);
                pacientes.setFecha_inscripcion(fecha_insc);
                pacientes.setTelefono(Integer.parseInt(tf_telefono.getText()));
                pacientes.setEmail(tf_email.getText());

                hibernateControlador.modificarPaciente(pacientes);
                vaciarFormPaciente();
                rellenarTabla();
            } else {
                return;
            }

        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Sin selección");
            alert.setHeaderText("No has seleccionado ningún paciente");
            alert.setContentText("Por favor, selecciona un paciente en la lista.");
            alert.showAndWait();
        }
    }


    /* FIN FUNCIONES LAYOUT PACIENTES */


    /* FUNCIONES LAYOUT CITAS */
    private void rellenarComboPacienteCitas(){
        ArrayList listaPacientes = new ArrayList();
        for (Pacientes pacientes : hibernateControlador.getPaciente()){
            listaPacientes.add(pacientes.getNombre() + " " + pacientes.getApellido());
        }

        cb_pacientes_cita.setItems(FXCollections.observableArrayList(listaPacientes));

    }

    private void insertarCita(){
        if (cb_pacientes_cita.getSelectionModel().getSelectedItem() == null || dp_insertarCita_fecha.getValue() == null || ta_descripcion_insertarCita.getText().equalsIgnoreCase("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error enviando datos");
            alert.setHeaderText("Hay campos sin rellenar");
            alert.setContentText("Por favor, rellena todos los campos");
            alert.showAndWait();
            return;
        }

        LocalDate localDate = dp_insertarCita_fecha.getValue();
        Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
        Date fecha = Date.from(instant);

        for (Pacientes pacienteslist : hibernateControlador.getPaciente()){
            String nombre = pacienteslist.getNombre() + " " + pacienteslist.getApellido();
            if (nombre.equalsIgnoreCase(String.valueOf(cb_pacientes_cita.getSelectionModel().getSelectedItem()))){
                citas = new Citas();
                citas.setDescripcion(ta_descripcion_insertarCita.getText());
                citas.setFecha(fecha);
                citas.setPacientes(pacienteslist);

                hibernateControlador.guardarCita(citas);
                vaciarFormCitas();
                rellenarTabla();

            }
        }

    }

    private void rellenarListCitas(){
        dp_cita_desde.valueProperty().setValue(null);
        ObservableList data = FXCollections.observableList(hibernateControlador.getCitas());
        if (data!=null){
            lv_citas.setItems(data);

        }

    }

    private void vaciarFormCitas(){
        cb_pacientes_cita.getSelectionModel().select(null);
        ta_descripcion_insertarCita.setText("");
        dp_insertarCita_fecha.setValue(null);
    }

    private void buscarCita(){
        LocalDate localDate_fechaDesde = dp_cita_desde.getValue();
        Instant instant_desde = Instant.from(localDate_fechaDesde.atStartOfDay(ZoneId.systemDefault()));
        fecha_desde_cita = Date.from(instant_desde);

        for (Citas citas : hibernateControlador.getCitas()){
            ObservableList data = FXCollections.observableList(hibernateControlador.getCitasBuscar(fecha_desde_cita));
            if (data!=null){
                lv_citas.setItems(data);

            }
        }

    }

    private void rellenarFechas() {

        final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);

                        int dia_item = item.getDayOfMonth();
                        Month mes_item = item.getMonth();
                        int ano_item = item.getYear();

                        for (Citas citas : hibernateControlador.getCitas()) {

                            Date fecha = citas.getFecha();
                            LocalDate fecha2 = fecha.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                            int dia = fecha2.getDayOfMonth();
                            Month mes = fecha2.getMonth();
                            int ano = fecha2.getYear();

                            if (dia == dia_item && mes == mes_item && ano == ano_item) {
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    }
                };

            }

        };

        dp_cita_desde.setDayCellFactory(dayCellFactory);
        dp_insertarCita_fecha.setDayCellFactory(dayCellFactory);
    }

    private void eliminarCita(){
        Citas filaSeleccionada = (Citas) lv_citas.getSelectionModel().getSelectedItem();
        int numeroFila = lv_citas.getSelectionModel().getSelectedIndex();
        if (numeroFila >= 0) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Eliminar");
            alert.setHeaderText("Eliminar cita");
            alert.setContentText("¿Estás seguro?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                hibernateControlador.eliminarCita(filaSeleccionada);
                rellenarTabla();
                vaciarFormCitas();
            } else {
                return;
            }

        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Sin selección");
            alert.setHeaderText("No has seleccionado ninguna cita");
            alert.setContentText("Por favor, selecciona una cita en la lista.");
            alert.showAndWait();
        }
    }

    private void verHoyCitas(){
        Date dia_actual = new Date();
        LocalDate localDate_dia_actual = LocalDate.now();
        Instant instant_desde = Instant.from(localDate_dia_actual.atStartOfDay(ZoneId.systemDefault()));
        dia_actual = Date.from(instant_desde);

        for (Citas citas : hibernateControlador.getCitas()){
            ObservableList data = FXCollections.observableList(hibernateControlador.getCitasBuscar(dia_actual));
            if (data!=null){
                lv_citas.setItems(data);

            }
        }
    }

    /* FIN FUNCIONES LAYOUT CITAS */


    @FXML
    private void irConexion() {
        layout_acercade.setVisible(false);
        layout_inicio.setVisible(false);
        layout_calendario.setVisible(false);
        layout_pacientes.setVisible(false);

    }

    @FXML
    private void irAcercade() {
        layout_acercade.setVisible(true);
        layout_inicio.setVisible(false);
        layout_calendario.setVisible(false);
        layout_pacientes.setVisible(false);

    }


    @FXML
    private void irInicio() {
        layout_inicio.setVisible(true);
        layout_calendario.setVisible(false);
        layout_acercade.setVisible(false);
        layout_pacientes.setVisible(false);

    }

    @FXML
    private void irCalendario() {
        layout_calendario.setVisible(true);
        layout_inicio.setVisible(false);
        layout_acercade.setVisible(false);
        layout_pacientes.setVisible(false);

    }

    @FXML
    private void irPacientes() {
        layout_calendario.setVisible(false);
        layout_inicio.setVisible(false);
        layout_acercade.setVisible(false);
        layout_pacientes.setVisible(true);

    }

    private void comprobarConexion() {
        hibernateControlador = new HibernateControlador();
        hibernateControlador.conectar();

        if (hibernateControlador.poll() == false){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Conexión");
            alert.setHeaderText("Error de conexión");
            alert.setContentText("Comprueba el fichero de configuración o revise el apartado de conexión en el manual de usuario.");
            alert.showAndWait();
            System.exit(0);
        } else {
            rellenarTabla();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Conexión");
            alert.setHeaderText("Conectado con éxito");
            alert.showAndWait();
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        comprobarConexion();

        bt_acordeon_inicio.setOnAction(event -> irInicio());
        bt_acordeon_inicio.setText("");

        bt_acordeon_calendario.setOnAction(event -> irCalendario());
        bt_acordeon_acercade.setOnAction(event -> irAcercade());
        bt_acordeon_pacientes.setOnAction(event -> irPacientes());
        bt_anchor_inicio_calendario.setOnAction(event -> irCalendario());
        bt_anchor_inicio_acercade.setOnAction(event -> irAcercade());
        bt_anchor_inicio_pacientes.setOnAction(event -> irPacientes());

        bt_cerrar_programa.setOnAction(event -> CerrarPrograma());

        /* LAYOUT PACIENTES */
        // rellenarTabla();
        bt_vaciar_form_paciente.setOnAction(event -> vaciarFormPaciente());
        bt_vaciar_form_paciente.setTooltip(new Tooltip("Vaciar formulario"));
        bt_insertar_paciente.setOnAction(event -> insertarPaciente());
        bt_insertar_paciente.setTooltip(new Tooltip("Añadir nuevo paciente"));
        bt_eliminar_fila_paciente.setOnAction(event -> eliminarPaciente());
        bt_eliminar_fila_paciente.setTooltip(new Tooltip("Eliminar paciente"));
        bt_modificar_fila_paciente.setOnAction(event -> modificarPaciente());
        bt_modificar_fila_paciente.setTooltip(new Tooltip("Modificar paciente"));

        tabla_pacientes.getSelectionModel().select(null);

        tabla_pacientes.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(tabla_pacientes.getSelectionModel().getSelectedItem() != null){

                Pacientes filaseleccionada = (Pacientes) tabla_pacientes.getSelectionModel().getSelectedItem();
                tf_nombre_paciente.setText(filaseleccionada.getNombre());
                tf_apellidos.setText(filaseleccionada.getApellido());

                Date fecha_paracombo = filaseleccionada.getFecha_nacimiento();
                LocalDate fecha_nacimiento = fecha_paracombo.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                dp_nacimiento.setValue(fecha_nacimiento);
                tf_email.setText(filaseleccionada.getEmail());
                tf_telefono.setText(String.valueOf(filaseleccionada.getTelefono()));
            }
        });

        /* FIN LAYOUT PACIENTES */


        /* LAYOUT CITAS */
        bt_aceptar_insertarCita.setOnAction(event -> insertarCita());
        bt_vaciar_insertarCita.setOnAction(event -> vaciarFormCitas());
        bt_vertodo_citas.setOnAction(event -> rellenarListCitas());

        dp_cita_desde.valueProperty().addListener(new ChangeListener<LocalDate>() {
            @Override
            public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {
                buscarCita();
            }
        });

        bt_eliminar_cita.setOnAction(event -> eliminarCita());
        bt_verhoy_citas.setOnAction(event -> verHoyCitas());

        /* FIN LAYOUT CITAS */

    }

    private ObservableList getInitialTableData() {
        ObservableList data = FXCollections.observableList(hibernateControlador.getPaciente());
        return data;

    }

    private void rellenarTabla(){
        rellenarComboPacienteCitas();
        rellenarListCitas();
        rellenarFechas();
        data = getInitialTableData();
        tabla_pacientes.setItems(data);

        col_nombre_paciente.setCellValueFactory(new PropertyValueFactory("nombre"));
        col_apellidos_paciente.setCellValueFactory(new PropertyValueFactory("apellido"));
        col_fecha_inscr.setCellFactory(new FormatFechas(new SimpleDateFormat("dd MMM YYYY")));
        col_fecha_inscr.setCellValueFactory( new PropertyValueFactory("fecha_inscripcion"));
        col_fecha_paciente.setCellFactory(new FormatFechas(new SimpleDateFormat("dd MMM YYYY")));
        col_fecha_paciente.setCellValueFactory(new PropertyValueFactory("fecha_nacimiento"));
        col_email_paciente.setCellValueFactory(new PropertyValueFactory("email"));
        col_tf_paciente.setCellValueFactory(new PropertyValueFactory("telefono"));
        tabla_pacientes.getColumns().setAll(col_nombre_paciente, col_apellidos_paciente, col_fecha_inscr, col_fecha_paciente, col_email_paciente, col_tf_paciente);

    }
}
