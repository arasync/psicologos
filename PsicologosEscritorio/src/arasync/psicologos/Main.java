package arasync.psicologos;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        Parent root = FXMLLoader.load(Main.class.getResource("/recursos/psicologos_view.fxml"));
        primaryStage.setOnCloseRequest(event -> System.exit(0));
        primaryStage.setResizable(false);
        primaryStage.setTitle("Arasync - Software para psicólogos");

        Scene scene = new Scene(root, 1073, 651);
        scene.getStylesheets().add(Main.class.getResource("/recursos/estilos.css").toExternalForm());
        primaryStage.setScene(scene);
        //primaryStage.setScene(new Scene(root, 1157, 761));

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
