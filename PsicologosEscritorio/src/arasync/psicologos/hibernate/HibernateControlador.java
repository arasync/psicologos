package arasync.psicologos.hibernate;

import arasync.psicologos.entidades.Citas;
import arasync.psicologos.entidades.Pacientes;
import com.mysql.jdbc.exceptions.MySQLSyntaxErrorException;
import javafx.scene.control.Alert;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.exception.SQLGrammarException;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.hibernate.service.spi.ServiceException;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import static arasync.psicologos.util.VariablesGlobales.RUTA_FICHERO;


/**
 * Created by Juan on 27/02/2017.
 */
public class HibernateControlador {
    private static SessionFactory sessionFactory;
    public static Session session;

    public void conectar() {
        Properties prop = new Properties();
        InputStream in = null;

            try {
                in = new FileInputStream(RUTA_FICHERO);
                prop.load(in);

            } catch (FileNotFoundException e) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Fichero no encontrado");
                alert.setHeaderText("Debe crear primero el fichero de conexión en configuración");
                alert.showAndWait();
                System.exit(0);

            } catch (IOException e) {

            }

        prop.getProperty("hibernate.connection.username");
        prop.getProperty("hibernate.connection.password");
        prop.getProperty("hibernate.connection.url");

        try {
            Configuration configuration = new Configuration();
            configuration.configure().
            setProperties(prop).
            configure("hibernate.cfg.xml");

            ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.setProperties(prop).
                    getProperties()).buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);

            session = sessionFactory.openSession();

        }catch (ServiceException ms){
            ms.printStackTrace();

        } catch (HibernateException e) {
            e.printStackTrace();
        }
    }

    public static Session getCurrentSession() {

        if (!session.isOpen())
           session = sessionFactory.openSession();

        return session;
    }

    public void guardarPaciente(Pacientes pacientes) {
        Session sesion = getCurrentSession();
        sesion.beginTransaction();
        sesion.save(pacientes);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void guardarCita(Citas citas) {
        Session sesion = getCurrentSession();
        sesion.beginTransaction();
        sesion.save(citas);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void eliminarPaciente(Pacientes pacientes){
        Session sesion = getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(pacientes);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void eliminarCita(Citas citas){
        Session sesion = getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(citas);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarPaciente(Pacientes pacientes){
        Session sesion = getCurrentSession();
        sesion.beginTransaction();
        sesion.update(pacientes);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public ArrayList<Pacientes> getPaciente() {
        Query query = getCurrentSession().
                createQuery("FROM Pacientes ORDER BY nombre");
        ArrayList<Pacientes> pacientes = (ArrayList<Pacientes>) query.list();
        return pacientes;
    }

    public ArrayList<Citas> getCitas() {
        Query query = getCurrentSession().
                createQuery("FROM Citas ORDER BY fecha");
        ArrayList<Citas> citas = (ArrayList<Citas>) query.list();
        return citas;
    }

    public ArrayList<Citas> getCitasBuscar(Date fecha) {
        Query query = getCurrentSession().
                createQuery("FROM Citas WHERE fecha = :fecha ORDER BY fecha")
                .setParameter("fecha", fecha);

        ArrayList<Citas> citas = (ArrayList<Citas>) query.list();
        return citas;

    }

    //Método para comprobar si recibimos respuesta haciendo una operación con hibernate y recibiendo
    //un booleano falso en la excepción de la conexión.
     public static boolean poll() {
        boolean connected = true;
        try {
            final Session session = getCurrentSession();
            final Transaction tx = session.beginTransaction();
            tx.commit();
            session.close();
        } catch (JDBCConnectionException ex) {
            connected = false;
        } catch (SQLGrammarException ge){
            connected = false;
        }

         return connected;
    }
}