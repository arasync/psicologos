# README #

Aplicación para la gestión de un centro de psicología

### Funciones del programa ###

* Ver, modificar, eliminar pacientes, tareas, y más
* Informes
* Comprobar estado de pacientes

### ¿Cómo iniciar el programa? ###

* Crear base de datos
* Importar base de datos
* Ejecutar JAR